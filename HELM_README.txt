HELM/NNAMDI: CI for Open Source Project
Step 1. 
a).	Forked the github repo:  https://github.com/facebookresearch/ParlAI as instructed to my local desktop repository, and setup my text editing environment using Visual Studio Code.
b).	Created files namely the yml pipeline file: Dockerfile.Helm, HELM_README.txt. Dockerfile.Helm used for building of a new image atop of python as base image, binaries, and dependencies. HELM_README.txt doc here including all that have been done in this project. I initially choose the ‘latest’ Python Docker image as my base image. 
Step 2.
a). 	Cloned the forked repo from my local to Bitbucket and Committed the files I created from Visual Studio Code to my newly created Bitbucket repository.
b). 	Built the Bitbucket Pipeline which included the code testing step, Code build, and Image push steps, to DockerHub.
Step 3.
a). 	Edited the source code in Bitbucket to proper format and committed, then checked the Pipeline option and run the pipeline beginning from the test step.
b). 	Troubleshooted the resultant summary of from the console output. This process was repeated for as many as five times following each subsequent test failure in attempt to eliminate all infrastructure and scripting deficiencies.  
C).	    Added Charltonaustin as a github user to my repository.

my Thoughts: I choose the python:3.9 which is the latest python image but late downgraded it with realization that it is still unstable for the ParlAi app based of some of the error’s messages during testing. Other failed tests indicated the necessity of building a docker image for ParlAi with all libraries, binaries, and all dependencies a feat I tried to achieve by building from scratch upon which to run this docker container as proposed.     
	         Most lines of code I included on this pipeline were an answer to the failed error messages of my test’s sessions. In my view, much of what is being requested going forward will, are by nature needs to be reviewed by the developers.
	         On the infrastructure perspective, I suppose a machine with larger CPU, higher memory capacity would be needed for a quicker process of each run. I needed more time to dig deeper but challenged by other scheduled responsibilities.

Personal Challenges: 
1.	I had to setup a bitbucket account after several attempt to recover my old account.
2.	Running the test script of this pipeline was a long process, It took a lot time to run and worse still when it had to fail.
3.	I discovered the need for a base image that has the dependencies already install, but since I could not find any in the repository, I had to build these by myself from scratch.
4.	I feel It could be faster if it were to run on a faster CPU or on higher memory machine.
5.	I run the docker build to create an artifact which was extremely slow at all stages 
6.	I enjoyed the project – it was a good practice to hands on with some  different pipeline with git  bitbucket.
